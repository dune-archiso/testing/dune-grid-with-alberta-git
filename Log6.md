21000b0d1cc5dd127dea4c342dcbca23f4673f93 is the first bad commit
commit 21000b0d1cc5dd127dea4c342dcbca23f4673f93
Author: Tiago Stürmer Daitx <tiago.daitx@ubuntu.com>
Date:   Mon Jan 11 00:00:00 2021 +0100

    Description: add rpc library
    
    Sun RPC is no longer part of glibc and should be replaced by TI-RPC.
    In order to build it configure.ac must check for the tirpc library.

 configure.ac | 2 ++
 1 file changed, 2 insertions(+)