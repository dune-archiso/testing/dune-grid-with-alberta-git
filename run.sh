#!/usr/bin/env bash

clear
echo ${BASH_VERSION}

function upgrade() {
  sudo pacman -Sy --needed --noconfirm # yu
}

function clean() {
  rm -rf /tmp/makepkg
}

function make_alberta() {
  local PACKAGE=alberta-git
  pushd ${PACKAGE}
  echo "Hello from ${PWD}!"
  rm -f ${PACKAGE}-*-x86_64.pkg.tar.zst
  upgrade
  sudo pacman -Rscn --noconfirm ${PACKAGE} >/dev/null 2>&1
  makepkg --noconfirm -src # --nocheck
  sudo pacman --noconfirm -U ${PACKAGE}-*-x86_64.pkg.tar.zst
  popd
  clean
}

function make_dune_grid-git() {
  local PACKAGE=dune-grid-git
  pushd ${PACKAGE}
  echo "Hello from ${PWD}!"
  rm -f ${PACKAGE}-*-x86_64.pkg.tar.zst
  upgrade
  makepkg --noconfirm -src # --nocheck
  popd
  clean
}

function generate_list_of_commits() {
  # https://stackoverflow.com/a/6703259
  # https://stackoverflow.com/a/1441062/9302545
  rm -f alberta-commit*.history
  pushd alberta
  git log --pretty=oneline --since=5.months >../alberta-commitv1.history
  git log --pretty=format:"%h%x09%an%x09%ad%x09%s" --date=short --since=4.months >../alberta-commitv2.history
  popd
}

# generate_list_of_commits
make_alberta
# make_dune_grid-git
cd ${GITPOD_REPO_ROOT}
