```console
$ cd alberta
$ git bisect start
$ git bisect bad
$ git checkout 369bb16423fdbe12c66bc843a30921b8a16cc9ee # with the patch!
Previous HEAD position was ae83e4d Merge branch 'feature/drop-gnucompat' into 'main'
HEAD is now at 369bb16 Fix compilation for openSuse
$ git bisect good
Bisecting: 46 revisions left to test after this (roughly 6 steps)
[3c6d565b0b5d49f0b5462ec5cd976eb859960404] Ignore failing tests for Debian and Ubuntu
$ git bisect bad
Bisecting: 21 revisions left to test after this (roughly 5 steps)
[c08e11105dc3ee1e926c432aa630a5144e7288f3] Adjust type to match definition
$ git bisect bad
Bisecting: 10 revisions left to test after this (roughly 4 steps)
[07101d1b951a7d6464993420772501345d56248c] [automake][autoconf] Include new folder book-demo
$ git bisect bad
Bisecting: 5 revisions left to test after this (roughly 3 steps)
[3f73f1292a14426efe188e5c6e176e8943fc24f8] [automake] Use make check for executalbes in alberta/test
$ git bisect bad
Bisecting: 2 revisions left to test after this (roughly 1 step)
[4a9b6d81d1ba84ab1158e6ba386e2f12687379db] Merge branch 'feature/multi-distro-ci' into 'main'
$ git bisect bad
Bisecting: 0 revisions left to test after this (roughly 0 steps)
$ git bisect bad
[727b0aaef33a801ebb108c1db07d181439a087df] [autoconf] Add include path to tirpc headers
727b0aaef33a801ebb108c1db07d181439a087df is the first bad commit
commit 727b0aaef33a801ebb108c1db07d181439a087df
Author: Christoph Grüninger <foss@grueninger.de>
Date:   Sun Apr 10 23:35:05 2022 +0200

    [autoconf] Add include path to tirpc headers
    
    Otherwise Debian 11 and Ubuntu is not able to find
    tirpc/rpc/xdr.h and tirpc/rpc/types.h.
    
    Workaround for issue #4.

 .gitlab-ci.yml | 12 ++++++------
 configure.ac   |  6 +++---
 2 files changed, 9 insertions(+), 9 deletions(-)
```

<details>
  <summary>
  `5829bd7ea4f6e1d7b34a9225a5371d945530b29b` ❌
  </summary>

```
[ 78%] Linking CXX executable test-dgf-alberta
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-alberta.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-alberta.dir/test-dgf-alberta.cc.o" -o test-dgf-alberta  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../../lib/libdunealbertagrid2d.so ../../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/build.make:110: dune/grid/io/file/dgfparser/test/test-dgf-alberta] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9104: dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/all] Error 2
make[2]: *** Waiting for unfinished jobs....
[ 78%] Linking CXX executable test-oned
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-oned.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-oned.dir/test-oned.cc.o" -o test-oned  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
[ 78%] Linking CXX executable test-dgf-gmsh-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-gmsh-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-gmsh-ug.dir/test-dgf-ug.cc.o" -o test-dgf-gmsh-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-oned
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-dgf-gmsh-ug
[ 82%] Linking CXX executable test-yaspgrid-yaspfactory-1d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/test-yaspgrid-yaspfactory-1d.cc.o" -o test-yaspgrid-yaspfactory-1d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-yaspgrid-yaspfactory-1d
[ 82%] Linking CXX executable gmshtest-alberta3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/gmshtest-alberta3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-alberta3d.dir/gmshtest.cc.o" -o gmshtest-alberta3d  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../lib/libdunealbertagrid3d.so ../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
[ 82%] Linking CXX executable issue-53-uggrid-intersections
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/issue-53-uggrid-intersections.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o" -o issue-53-uggrid-intersections  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/build.make:110: dune/grid/io/file/test/gmshtest-alberta3d] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:8999: dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/all] Error 2
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target issue-53-uggrid-intersections
[ 82%] Linking CXX executable test-parallel-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-parallel-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-parallel-ug.dir/test-parallel-ug.cc.o" -o test-parallel-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-parallel-ug
[ 86%] Linking CXX executable structuredgridfactorytest
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/utility/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/structuredgridfactorytest.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread CMakeFiles/structuredgridfactorytest.dir/structuredgridfactorytest.cc.o -o structuredgridfactorytest  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
[ 86%] Linking CXX executable test-alberta-1-3
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-1-3.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-1-3.dir/test-alberta.cc.o" -o test-alberta-1-3  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid3d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
[ 86%] Built target structuredgridfactorytest
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-1-3.dir/build.make:110: dune/grid/test/test-alberta-1-3] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9680: dune/grid/test/CMakeFiles/test-alberta-1-3.dir/all] Error 2
[ 86%] Linking CXX executable test-alberta-1-2
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-1-2.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o" -o test-alberta-1-2  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid2d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build.make:110: dune/grid/test/test-alberta-1-2] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9626: dune/grid/test/CMakeFiles/test-alberta-1-2.dir/all] Error 2
[ 86%] Linking CXX executable test-yaspgrid-yaspfactory-2d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/test-yaspgrid-yaspfactory-2d.cc.o" -o test-yaspgrid-yaspfactory-2d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-yaspgrid-yaspfactory-2d
[ 91%] Linking CXX executable test-alberta-2-2
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-2-2.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-2-2.dir/test-alberta.cc.o" -o test-alberta-2-2  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid2d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-2-2.dir/build.make:110: dune/grid/test/test-alberta-2-2] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9653: dune/grid/test/CMakeFiles/test-alberta-2-2.dir/all] Error 2
[ 91%] Linking CXX executable test-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-ug.dir/test-ug.cc.o" -o test-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-ug
[ 91%] Linking CXX executable test-yaspgrid-yaspfactory-3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/test-yaspgrid-yaspfactory-3d.cc.o" -o test-yaspgrid-yaspfactory-3d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-yaspgrid-yaspfactory-3d
[ 91%] Linking CXX executable test-geogrid-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-uggrid.dir/test-geogrid.cc.o" -o test-geogrid-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-geogrid-uggrid
[ 91%] Linking CXX executable test-geogrid-yaspgrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-yaspgrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-yaspgrid.dir/test-geogrid.cc.o" -o test-geogrid-yaspgrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-geogrid-yaspgrid
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1822: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:546: build_tests] Error 2
==> ERROR: A failure occurred in check().
```

</details>

<details>
  <summary>
  `c08e11105dc3ee1e926c432aa630a5144e7288f3` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
```

</details>

<details>
  <summary>
  `07101d1b951a7d6464993420772501345d56248c` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
```

<details>
  <summary>
  `3f73f1292a14426efe188e5c6e176e8943fc24f8` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
```

</details>

<details>
  <summary>
  `4a9b6d81d1ba84ab1158e6ba386e2f12687379db` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
compilation terminated.
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
==> Removing installed dependencies...
```

<details>
  <summary>
  `727b0aaef33a801ebb108c1db07d181439a087df` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
compilation terminated.
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
==> Removing installed dependencies...
checking dependencies...
```

We did a mistake since that sed was not patching 😅.