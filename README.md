## 📃 Solution

We find in [Log2.md](Log2.md) that `alberta` changed the headers `#include <alberta/alberta_util.h>` to `#include <alberta_util.h>`.

[`369bb16423fdbe12c66bc843a30921b8a16cc9ee`](https://gitlab.com/alberta-fem/alberta3/-/commit/369bb16423fdbe12c66bc843a30921b8a16cc9ee)
[`5829bd7ea4f6e1d7b34a9225a5371d945530b29b`](https://gitlab.com/alberta-fem/alberta3/-/commit/5829bd7ea4f6e1d7b34a9225a5371d945530b29b)

Then, when the changed "was reverted" it hidden between exactly 83 commits.

The solution will be immediately patch the commit that changed the header
and apply `git bisect` to catch the bug that causes `dune-grid` tests with `alberta` to fail.

```console
$ cd alberta
$ git bisect start
$ git bisect bad
$ git checkout b1cc38b1bb01514b46c3dff90f8c82817e05cca1
$ git bisect good
```

```console
alberta-git W: Referenced library 'sh' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_utilities.so.4' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_fem_2d.so.4' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_fem_1d.so.4' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_fem_3d.so.4' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_2d.so.4' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_1d.so.4' is an uninstalled dependency
alberta-git W: Referenced library 'libalberta_3d.so.4' is an uninstalled dependency
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/bin/alberta2gmv1d')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/bin/alberta2gmv2d')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/bin/alberta2gmv3d')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/bin/alberta2oogl')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalbas_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalbas_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalbas_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalbas_2d.so.0.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalbas_2d.so.0.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalbas_2d.so.0.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalbas_3d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalbas_3d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalbas_3d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalberta_fem_1d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_fem_1d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalberta_fem_2d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_fem_2d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalberta_fem_3d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_fem_3d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalberta_fem_4d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_fem_4d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libltdl.so.7' by file ('usr/lib/libalberta_fem_5d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_fem_5d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_gfx_1d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalberta_gfx_1d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_gfx_2d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalberta_gfx_2d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_gfx_3d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalberta_gfx_3d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_gfx_4d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalberta_gfx_4d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_gfx_5d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libalberta_gfx_5d.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libalberta_utilities.so.4.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/liboem_block_solve_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/liboem_block_solve_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/liboem_block_solve_2d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/liboem_block_solve_2d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/liboem_block_solve_3d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/liboem_block_solve_3d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libstatic_condensation_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libstatic_condensation_1d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libstatic_condensation_2d.so.0.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libstatic_condensation_2d.so.0.0.0')
alberta-git W: Unused shared library '/usr/lib/libtirpc.so.3' by file ('usr/lib/libstatic_condensation_3d.so.1.0.0')
alberta-git W: Unused shared library '/usr/lib/libm.so.6' by file ('usr/lib/libstatic_condensation_3d.so.1.0.0')
alberta-git E: Dependency libtirpc detected and not included (libraries ['usr/lib/libtirpc.so.3'] needed in files ['usr/lib/libstatic_condensation_3d.so.1.0.0'])
alberta-git E: Dependency libtool detected and not included (libraries ['usr/lib/libltdl.so.7'] needed in files ['usr/lib/libalberta_fem_5d.so.4.0.0'])
alberta-git W: Dependency included and not needed ('libx11')
```

[](https://archlinux.org/pacman/makepkg.8.html)
[](https://gt-user.globus.narkive.com/RNgzTAOB/5-2-0-build-error)
