```
369bb16423fdbe12c66bc843a30921b8a16cc9ee is the first bad commit
commit 369bb16423fdbe12c66bc843a30921b8a16cc9ee
Author: Christoph Grüninger <foss@grueninger.de>
Date:   Tue Apr 5 23:17:37 2022 +0200

    Fix compilation for openSuse
    
    Include path is not right.

 alberta/src/Common/alberta.h | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
```

```console
$ cd alberta
$ git bisect start
$ git bisect bad
$ git checkout ad0903d9fc88d023d5e68f09929a0a63d2e08dd3
$ git bisect good
```

<details>
  <summary>
  `3c6d565b0b5d49f0b5462ec5cd976eb859960404` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
```

</details>

<details>
  <summary>
  `3822f05379e9b0822e5f55a937727c22b7d3ac36` ❌
  </summary>

```

[ 85%] Building CXX object CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
```

</details>

<details>
  <summary>
  `369ed795d36a25210710c89924c2d487a971f65f` ❌
  </summary>

```
Makefile.am: installing './INSTALL'
configure.ac:34: error: required file 'demo/src/Makefile.in' not found
configure.ac:34: error: required file 'demo/src/1d/Makefile.in' not found
configure.ac:34: error: required file 'demo/src/2d/Makefile.in' not found
configure.ac:34: error: required file 'demo/src/3d/Makefile.in' not found
configure.ac:34: error: required file 'demo/src/4d/Makefile.in' not found
configure.ac:34: error: required file 'demo/src/5d/Makefile.in' not found
add_ons/bamg2alberta/Makefile.am: installing './depcomp'
add_ons/block_solve/demo/2d/Makefile.am:27: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/block_solve/src/Makefile.am:1: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/geomview/Makefile.am:44: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/gmv/1d/Makefile.am:46: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/gmv/2d/Makefile.am:46: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/gmv/3d/Makefile.am:47: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/grape/mesh/2d/Makefile.am:90: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/grape/mesh/3d/Makefile.am:90: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/libalbas/src/Makefile.am:1: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/libalbas/tests/Makefile.am:11: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/meshtv/1d/Makefile.am:41: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/meshtv/2d/Makefile.am:41: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/meshtv/3d/Makefile.am:41: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/paraview/2d/Makefile.am:47: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/paraview/3d/Makefile.am:47: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/static_condensation/demo/2d/Makefile.am:29: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/static_condensation/demo/3d/Makefile.am:28: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/static_condensation/src/Makefile.am:1: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
add_ons/triangle2alberta/Makefile.am:34: warning: 'INCLUDES' is the old name for 'AM_CPPFLAGS' (or '*_CPPFLAGS')
parallel-tests: installing './test-driver'
autoreconf: error: automake failed with exit status: 1
==> ERROR: A failure occurred in build().
    Aborting...
```

</details>

<details>
  <summary>
  `3c6d565b0b5d49f0b5462ec5cd976eb859960404` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
```

</details>

<details>
  <summary>
  `727b0aaef33a801ebb108c1db07d181439a087df` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=3 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid3d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:107: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2314: CMakeFiles/dunealbertagrid2d.dir/all] Error 2
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
```

</details>

<details>
  <summary>
  `369bb16423fdbe12c66bc843a30921b8a16cc9ee` ❌
  </summary>

```
[ 85%] Building CXX object CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc
/usr/sbin/g++ -DALBERTA_DIM=1 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid1d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc
/usr/sbin/g++ -DALBERTA_DIM=2 -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DHAVE_CONFIG_H -DModelP -Ddunealbertagrid2d_EXPORTS -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIC -pthread -std=gnu++17 -MD -MT CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -MF CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o.d -o CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:135: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/geometrycache.hh:6,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.hh:15,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/elementinfo.cc:12:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:93: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
compilation terminated.
make[2]: *** Waiting for unfinished jobs....
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:93: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
make[2]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:93: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/elementinfo.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:135: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:121: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/meshpointer.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:121: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.hh:14,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/macrodata.cc:17:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:135: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/meshpointer.cc.o] Error 1
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:121: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/macrodata.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:107: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:107: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/indexsets.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[2]: *** [CMakeFiles/dunealbertagrid1d.dir/build.make:79: CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/agrid.hh:35,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid.hh:5,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.hh:8,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/dgfparser.cc:10:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
make[1]: *** [CMakeFiles/Makefile2:2288: CMakeFiles/dunealbertagrid1d.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
make[2]: *** [CMakeFiles/dunealbertagrid3d.dir/build.make:79: CMakeFiles/dunealbertagrid3d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2340: CMakeFiles/dunealbertagrid3d.dir/all] Error 2
make[2]: *** [CMakeFiles/dunealbertagrid2d.dir/build.make:79: CMakeFiles/dunealbertagrid2d.dir/dune/grid/albertagrid/dgfparser.cc.o] Error 1
In file included from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/albertaheader.hh:57,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/misc.hh:13,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.hh:16,
                 from /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/albertagrid/indexsets.cc:7:
/usr/include/alberta/alberta.h:51:10: fatal error: alberta_util.h: No such file or directory
   51 | #include <alberta_util.h>
      |          ^~~~~~~~~~~~~~~~
compilation terminated.
```

</details>