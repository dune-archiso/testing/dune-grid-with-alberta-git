```console
$ cd alberta
$ git bisect start
$ git bisect bad
$ git checkout 369bb16423fdbe12c66bc843a30921b8a16cc9ee # with the patch!
Previous HEAD position was ae83e4d Merge branch 'feature/drop-gnucompat' into 'main'
HEAD is now at 369bb16 Fix compilation for openSuse
$ git bisect good
Bisecting: 46 revisions left to test after this (roughly 6 steps)
[3c6d565b0b5d49f0b5462ec5cd976eb859960404] Ignore failing tests for Debian and Ubuntu
$ git bisect bad
Bisecting: 21 revisions left to test after this (roughly 5 steps)
[c08e11105dc3ee1e926c432aa630a5144e7288f3] Adjust type to match definition
$ git bisect bad
Bisecting: 10 revisions left to test after this (roughly 4 steps)
[07101d1b951a7d6464993420772501345d56248c] [automake][autoconf] Include new folder book-demo
$ git bisect bad
Bisecting: 5 revisions left to test after this (roughly 3 steps)
[3f73f1292a14426efe188e5c6e176e8943fc24f8] [automake] Use make check for executalbes in alberta/test
$ git bisect bad
Bisecting: 2 revisions left to test after this (roughly 1 step)
[4a9b6d81d1ba84ab1158e6ba386e2f12687379db] Merge branch 'feature/multi-distro-ci' into 'main'
$ git bisect good
Bisecting: 0 revisions left to test after this (roughly 1 step)
[ff1a5e1fd1ec383eece853be89843a9f87e165b9] [autoconf] Check unconditionally for ltdl, add to tests
$ git bisect bad
Bisecting: 0 revisions left to test after this (roughly 0 steps)
[75fb27baa3dc38c3be3dcf3a42086b3e4162c180] Reenable compilation of testing code
$ git bisect good
ff1a5e1fd1ec383eece853be89843a9f87e165b9 is the first bad commit
commit ff1a5e1fd1ec383eece853be89843a9f87e165b9
Author: Christoph Grüninger <foss@grueninger.de>
Date:   Tue Apr 12 22:11:38 2022 +0200

    [autoconf] Check unconditionally for ltdl, add to tests
    
    Otherwise building the tests fail for Debian and Ubuntu

 alberta/tests/Makefile.am | 26 +++++++++++++-------------
 configure.ac              | 18 +++++++++---------
 2 files changed, 22 insertions(+), 22 deletions(-)
```

<details>
  <summary>
  `3c6d565b0b5d49f0b5462ec5cd976eb859960404` ❌
  </summary>

```
[ 73%] Linking CXX executable test-dgf-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-ug.dir/test-dgf-ug.cc.o" -o test-dgf-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/build.make:110: dune/grid/io/file/dgfparser/test/test-dgf-alberta] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9104: dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/all] Error 2
make[2]: *** Waiting for unfinished jobs....
[ 73%] Built target tensorgridfactorytest
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target gmshtest-uggrid
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target test-dgf-ug
[ 73%] Linking CXX executable test-loadbalancing
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-loadbalancing.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-loadbalancing.dir/test-loadbalancing.cc.o" -o test-loadbalancing  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target test-loadbalancing
[ 78%] Linking CXX executable test-yaspgrid-yaspfactory-1d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/test-yaspgrid-yaspfactory-1d.cc.o" -o test-yaspgrid-yaspfactory-1d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
[ 78%] Linking CXX executable test-dgf-gmsh-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-gmsh-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-gmsh-ug.dir/test-dgf-ug.cc.o" -o test-dgf-gmsh-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-yaspgrid-yaspfactory-1d
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-dgf-gmsh-ug
[ 78%] Linking CXX executable test-oned
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-oned.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-oned.dir/test-oned.cc.o" -o test-oned  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-oned
[ 78%] Linking CXX executable gmshtest-alberta3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/gmshtest-alberta3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-alberta3d.dir/gmshtest.cc.o" -o gmshtest-alberta3d  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../lib/libdunealbertagrid3d.so ../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/build.make:110: dune/grid/io/file/test/gmshtest-alberta3d] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:8999: dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/all] Error 2
[ 78%] Linking CXX executable test-parallel-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-parallel-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-parallel-ug.dir/test-parallel-ug.cc.o" -o test-parallel-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-parallel-ug
[ 82%] Linking CXX executable structuredgridfactorytest
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/utility/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/structuredgridfactorytest.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread CMakeFiles/structuredgridfactorytest.dir/structuredgridfactorytest.cc.o -o structuredgridfactorytest  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target structuredgridfactorytest
[ 82%] Linking CXX executable test-yaspgrid-yaspfactory-2d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/test-yaspgrid-yaspfactory-2d.cc.o" -o test-yaspgrid-yaspfactory-2d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-yaspgrid-yaspfactory-2d
[ 82%] Linking CXX executable test-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-ug.dir/test-ug.cc.o" -o test-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-ug
[ 82%] Linking CXX executable test-yaspgrid-yaspfactory-3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/test-yaspgrid-yaspfactory-3d.cc.o" -o test-yaspgrid-yaspfactory-3d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-yaspgrid-yaspfactory-3d
[ 82%] Linking CXX executable test-geogrid-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-uggrid.dir/test-geogrid.cc.o" -o test-geogrid-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-geogrid-uggrid
[ 82%] Linking CXX executable test-geogrid-yaspgrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-yaspgrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-yaspgrid.dir/test-geogrid.cc.o" -o test-geogrid-yaspgrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-geogrid-yaspgrid
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1822: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:546: build_tests] Error 2
==> ERROR: A failure occurred in check().
    Aborting...
```

</details>

<details>
  <summary>
  `c08e11105dc3ee1e926c432aa630a5144e7288f3` ❌
  </summary>

```
[ 73%] Building CXX object dune/grid/test/CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/sbin/g++ -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DModelP -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/grid/test/CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o -MF CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o.d -o CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test/issue-53-uggrid-intersections.cc
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target test-dgf-yasp
/usr/sbin/make  -f CMakeFiles/dunealbertagrid1d.dir/build.make CMakeFiles/dunealbertagrid1d.dir/depend
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
cd /tmp/makepkg/dune-grid-git/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/dune-grid-git/src/dune-grid /tmp/makepkg/dune-grid-git/src/dune-grid /tmp/makepkg/dune-grid-git/src/build-cmake /tmp/makepkg/dune-grid-git/src/build-cmake /tmp/makepkg/dune-grid-git/src/build-cmake/CMakeFiles/dunealbertagrid1d.dir/DependInfo.cmake --color=
Dependencies file "CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/dgfparser.cc.o.d" is newer than depends file "/tmp/makepkg/dune-grid-git/src/build-cmake/CMakeFiles/dunealbertagrid1d.dir/compiler_depend.internal".
Dependencies file "CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/elementinfo.cc.o.d" is newer than depends file "/tmp/makepkg/dune-grid-git/src/build-cmake/CMakeFiles/dunealbertagrid1d.dir/compiler_depend.internal".
Dependencies file "CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/indexsets.cc.o.d" is newer than depends file "/tmp/makepkg/dune-grid-git/src/build-cmake/CMakeFiles/dunealbertagrid1d.dir/compiler_depend.internal".
Dependencies file "CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/macrodata.cc.o.d" is newer than depends file "/tmp/makepkg/dune-grid-git/src/build-cmake/CMakeFiles/dunealbertagrid1d.dir/compiler_depend.internal".
Dependencies file "CMakeFiles/dunealbertagrid1d.dir/dune/grid/albertagrid/meshpointer.cc.o.d" is newer than depends file "/tmp/makepkg/dune-grid-git/src/build-cmake/CMakeFiles/dunealbertagrid1d.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target dunealbertagrid1d
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/make  -f CMakeFiles/dunealbertagrid1d.dir/build.make CMakeFiles/dunealbertagrid1d.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[3]: Nothing to be done for 'CMakeFiles/dunealbertagrid1d.dir/build'.
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target dunealbertagrid1d
/usr/sbin/make  -f dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build.make dune/grid/test/CMakeFiles/test-alberta-1-2.dir/depend
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
cd /tmp/makepkg/dune-grid-git/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/dune-grid-git/src/dune-grid /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test /tmp/makepkg/dune-grid-git/src/build-cmake /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/CMakeFiles/test-alberta-1-2.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/make  -f dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build.make dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Building CXX object dune/grid/test/CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/sbin/g++ -DALBERTA_DIM=2 -DDUNE_GRID_EXAMPLE_GRIDS_PATH=\"/tmp/makepkg/dune-grid-git/src/dune-grid/doc/grids/\" -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DGRIDDIM=1 -DHAVE_CONFIG_H -DModelP -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIE -pthread -std=gnu++17 -MD -MT dune/grid/test/CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o -MF CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o.d -o CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test/test-alberta.cc
[ 78%] Linking CXX executable test-dgf-alberta
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-alberta.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-alberta.dir/test-dgf-alberta.cc.o" -o test-dgf-alberta  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../../lib/libdunealbertagrid2d.so ../../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
[ 78%] Linking CXX executable test-dgf-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-ug.dir/link.txt --verbose=1
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-ug.dir/test-dgf-ug.cc.o" -o test-dgf-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/build.make:110: dune/grid/io/file/dgfparser/test/test-dgf-alberta] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9104: dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/all] Error 2
make[2]: *** Waiting for unfinished jobs....
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-dgf-ug
[ 78%] Linking CXX executable test-loadbalancing
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-loadbalancing.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-loadbalancing.dir/test-loadbalancing.cc.o" -o test-loadbalancing  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-loadbalancing
[ 78%] Linking CXX executable test-dgf-gmsh-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-gmsh-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-gmsh-ug.dir/test-dgf-ug.cc.o" -o test-dgf-gmsh-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-dgf-gmsh-ug
[ 78%] Linking CXX executable test-oned
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-oned.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-oned.dir/test-oned.cc.o" -o test-oned  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-oned
[ 82%] Linking CXX executable test-yaspgrid-yaspfactory-1d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/test-yaspgrid-yaspfactory-1d.cc.o" -o test-yaspgrid-yaspfactory-1d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-yaspgrid-yaspfactory-1d
[ 82%] Linking CXX executable gmshtest-alberta3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/gmshtest-alberta3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-alberta3d.dir/gmshtest.cc.o" -o gmshtest-alberta3d  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../lib/libdunealbertagrid3d.so ../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
[ 82%] Linking CXX executable issue-53-uggrid-intersections
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/issue-53-uggrid-intersections.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o" -o issue-53-uggrid-intersections  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target issue-53-uggrid-intersections
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/build.make:110: dune/grid/io/file/test/gmshtest-alberta3d] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:8999: dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/all] Error 2
[ 82%] Linking CXX executable test-parallel-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-parallel-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-parallel-ug.dir/test-parallel-ug.cc.o" -o test-parallel-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-parallel-ug
[ 86%] Linking CXX executable structuredgridfactorytest
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/utility/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/structuredgridfactorytest.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread CMakeFiles/structuredgridfactorytest.dir/structuredgridfactorytest.cc.o -o structuredgridfactorytest  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target structuredgridfactorytest
[ 86%] Linking CXX executable test-alberta-1-2
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-1-2.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o" -o test-alberta-1-2  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid2d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build.make:110: dune/grid/test/test-alberta-1-2] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9626: dune/grid/test/CMakeFiles/test-alberta-1-2.dir/all] Error 2
[ 86%] Linking CXX executable test-yaspgrid-yaspfactory-2d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/test-yaspgrid-yaspfactory-2d.cc.o" -o test-yaspgrid-yaspfactory-2d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-yaspgrid-yaspfactory-2d
[ 86%] Linking CXX executable test-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-ug.dir/test-ug.cc.o" -o test-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-ug
[ 86%] Linking CXX executable test-yaspgrid-yaspfactory-3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/test-yaspgrid-yaspfactory-3d.cc.o" -o test-yaspgrid-yaspfactory-3d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-yaspgrid-yaspfactory-3d
[ 86%] Linking CXX executable test-geogrid-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-uggrid.dir/test-geogrid.cc.o" -o test-geogrid-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-geogrid-uggrid
[ 86%] Linking CXX executable test-geogrid-yaspgrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-yaspgrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-yaspgrid.dir/test-geogrid.cc.o" -o test-geogrid-yaspgrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-geogrid-yaspgrid
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1822: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:546: build_tests] Error 2
==> ERROR: A failure occurred in check().
    Aborting...
```

<details>
  <summary>
  `07101d1b951a7d6464993420772501345d56248c` ❌
  </summary>

```
Consolidate compiler generated dependencies of target dunealbertagrid1d
[ 73%] Building CXX object dune/grid/test/CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/sbin/g++ -DALBERTA_DIM=2 -DDUNE_GRID_EXAMPLE_GRIDS_PATH=\"/tmp/makepkg/dune-grid-git/src/dune-grid/doc/grids/\" -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DGRIDDIM=1 -DHAVE_CONFIG_H -DModelP -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIE -pthread -std=gnu++17 -MD -MT dune/grid/test/CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o -MF CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o.d -o CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test/test-alberta.cc
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/make  -f CMakeFiles/dunealbertagrid1d.dir/build.make CMakeFiles/dunealbertagrid1d.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[3]: Nothing to be done for 'CMakeFiles/dunealbertagrid1d.dir/build'.
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target dunealbertagrid1d
/usr/sbin/make  -f dune/grid/test/CMakeFiles/test-alberta-2-2.dir/build.make dune/grid/test/CMakeFiles/test-alberta-2-2.dir/depend
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
cd /tmp/makepkg/dune-grid-git/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/dune-grid-git/src/dune-grid /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test /tmp/makepkg/dune-grid-git/src/build-cmake /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/CMakeFiles/test-alberta-2-2.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/make  -f dune/grid/test/CMakeFiles/test-alberta-2-2.dir/build.make dune/grid/test/CMakeFiles/test-alberta-2-2.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Building CXX object dune/grid/test/CMakeFiles/test-alberta-2-2.dir/test-alberta.cc.o
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/sbin/g++ -DALBERTA_DIM=2 -DDUNE_GRID_EXAMPLE_GRIDS_PATH=\"/tmp/makepkg/dune-grid-git/src/dune-grid/doc/grids/\" -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DGRIDDIM=2 -DHAVE_CONFIG_H -DModelP -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIE -pthread -std=gnu++17 -MD -MT dune/grid/test/CMakeFiles/test-alberta-2-2.dir/test-alberta.cc.o -MF CMakeFiles/test-alberta-2-2.dir/test-alberta.cc.o.d -o CMakeFiles/test-alberta-2-2.dir/test-alberta.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test/test-alberta.cc
[ 78%] Linking CXX executable test-dgf-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-ug.dir/test-dgf-ug.cc.o" -o test-dgf-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-dgf-ug
/usr/sbin/make  -f dune/grid/test/CMakeFiles/test-alberta-1-3.dir/build.make dune/grid/test/CMakeFiles/test-alberta-1-3.dir/depend
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
cd /tmp/makepkg/dune-grid-git/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/dune-grid-git/src/dune-grid /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test /tmp/makepkg/dune-grid-git/src/build-cmake /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/CMakeFiles/test-alberta-1-3.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/make  -f dune/grid/test/CMakeFiles/test-alberta-1-3.dir/build.make dune/grid/test/CMakeFiles/test-alberta-1-3.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Building CXX object dune/grid/test/CMakeFiles/test-alberta-1-3.dir/test-alberta.cc.o
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/sbin/g++ -DALBERTA_DIM=3 -DDUNE_GRID_EXAMPLE_GRIDS_PATH=\"/tmp/makepkg/dune-grid-git/src/dune-grid/doc/grids/\" -DENABLE_ALBERTA=1 -DENABLE_MPI=1 -DGRIDDIM=1 -DHAVE_CONFIG_H -DModelP -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIE -pthread -std=gnu++17 -MD -MT dune/grid/test/CMakeFiles/test-alberta-1-3.dir/test-alberta.cc.o -MF CMakeFiles/test-alberta-1-3.dir/test-alberta.cc.o.d -o CMakeFiles/test-alberta-1-3.dir/test-alberta.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test/test-alberta.cc
[ 78%] Linking CXX executable test-dgf-alberta
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-alberta.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-alberta.dir/test-dgf-alberta.cc.o" -o test-dgf-alberta  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../../lib/libdunealbertagrid2d.so ../../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/build.make:110: dune/grid/io/file/dgfparser/test/test-dgf-alberta] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9104: dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/all] Error 2
make[2]: *** Waiting for unfinished jobs....
[ 78%] Linking CXX executable test-dgf-gmsh-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-gmsh-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-gmsh-ug.dir/test-dgf-ug.cc.o" -o test-dgf-gmsh-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-dgf-gmsh-ug
[ 82%] Linking CXX executable test-yaspgrid-yaspfactory-1d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/test-yaspgrid-yaspfactory-1d.cc.o" -o test-yaspgrid-yaspfactory-1d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-yaspgrid-yaspfactory-1d
[ 82%] Linking CXX executable test-oned
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-oned.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-oned.dir/test-oned.cc.o" -o test-oned  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
[ 82%] Linking CXX executable gmshtest-alberta3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/gmshtest-alberta3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-alberta3d.dir/gmshtest.cc.o" -o gmshtest-alberta3d  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../lib/libdunealbertagrid3d.so ../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-oned
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/build.make:110: dune/grid/io/file/test/gmshtest-alberta3d] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:8999: dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/all] Error 2
[ 82%] Linking CXX executable issue-53-uggrid-intersections
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/issue-53-uggrid-intersections.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o" -o issue-53-uggrid-intersections  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target issue-53-uggrid-intersections
[ 82%] Linking CXX executable test-parallel-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-parallel-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-parallel-ug.dir/test-parallel-ug.cc.o" -o test-parallel-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-parallel-ug
[ 86%] Linking CXX executable structuredgridfactorytest
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/utility/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/structuredgridfactorytest.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread CMakeFiles/structuredgridfactorytest.dir/structuredgridfactorytest.cc.o -o structuredgridfactorytest  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target structuredgridfactorytest
[ 86%] Linking CXX executable test-alberta-1-3
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-1-3.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-1-3.dir/test-alberta.cc.o" -o test-alberta-1-3  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid3d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
[ 86%] Linking CXX executable test-alberta-1-2
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-1-2.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o" -o test-alberta-1-2  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid2d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-1-3.dir/build.make:110: dune/grid/test/test-alberta-1-3] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9680: dune/grid/test/CMakeFiles/test-alberta-1-3.dir/all] Error 2
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build.make:110: dune/grid/test/test-alberta-1-2] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9626: dune/grid/test/CMakeFiles/test-alberta-1-2.dir/all] Error 2
[ 86%] Linking CXX executable test-yaspgrid-yaspfactory-2d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/test-yaspgrid-yaspfactory-2d.cc.o" -o test-yaspgrid-yaspfactory-2d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-yaspgrid-yaspfactory-2d
[ 91%] Linking CXX executable test-alberta-2-2
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-2-2.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-2-2.dir/test-alberta.cc.o" -o test-alberta-2-2  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid2d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-2-2.dir/build.make:110: dune/grid/test/test-alberta-2-2] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9653: dune/grid/test/CMakeFiles/test-alberta-2-2.dir/all] Error 2
[ 91%] Linking CXX executable test-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-ug.dir/test-ug.cc.o" -o test-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-ug
[ 91%] Linking CXX executable test-yaspgrid-yaspfactory-3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/test-yaspgrid-yaspfactory-3d.cc.o" -o test-yaspgrid-yaspfactory-3d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-yaspgrid-yaspfactory-3d
[ 91%] Linking CXX executable test-geogrid-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-uggrid.dir/test-geogrid.cc.o" -o test-geogrid-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-geogrid-uggrid
[ 91%] Linking CXX executable test-geogrid-yaspgrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-yaspgrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-yaspgrid.dir/test-geogrid.cc.o" -o test-geogrid-yaspgrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 91%] Built target test-geogrid-yaspgrid
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1822: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:546: build_tests] Error 2
==> ERROR: A failure occurred in check().
    Aborting...
```

<details>
  <summary>
  `3f73f1292a14426efe188e5c6e176e8943fc24f8` ❌
  </summary>

```
[ 82%] Linking CXX executable issue-53-uggrid-intersections
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/issue-53-uggrid-intersections.dir/link.txt --verbose=1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-alberta3d.dir/gmshtest.cc.o" -o gmshtest-alberta3d  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../lib/libdunealbertagrid3d.so ../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o" -o issue-53-uggrid-intersections  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
[ 82%] Built target test-yaspgrid-yaspfactory-1d
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
[ 82%] Built target issue-53-uggrid-intersections
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/build.make:110: dune/grid/io/file/test/gmshtest-alberta3d] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:8999: dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/all] Error 2
[ 82%] Linking CXX executable test-dgf-gmsh-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-gmsh-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-gmsh-ug.dir/test-dgf-ug.cc.o" -o test-dgf-gmsh-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-dgf-gmsh-ug
[ 82%] Linking CXX executable test-parallel-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-parallel-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-parallel-ug.dir/test-parallel-ug.cc.o" -o test-parallel-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-parallel-ug
[ 82%] Linking CXX executable test-alberta-1-2
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-alberta-1-2.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-alberta-1-2.dir/test-alberta.cc.o" -o test-alberta-1-2  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../lib/libdunealbertagrid2d.so ../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_2d.so /usr/lib/libalberta_utilities.so 
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/test/CMakeFiles/test-alberta-1-2.dir/build.make:110: dune/grid/test/test-alberta-1-2] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9626: dune/grid/test/CMakeFiles/test-alberta-1-2.dir/all] Error 2
[ 86%] Linking CXX executable structuredgridfactorytest
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/utility/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/structuredgridfactorytest.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread CMakeFiles/structuredgridfactorytest.dir/structuredgridfactorytest.cc.o -o structuredgridfactorytest  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target structuredgridfactorytest
[ 86%] Linking CXX executable test-yaspgrid-yaspfactory-2d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/test-yaspgrid-yaspfactory-2d.cc.o" -o test-yaspgrid-yaspfactory-2d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-yaspgrid-yaspfactory-2d
[ 86%] Linking CXX executable test-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-ug.dir/test-ug.cc.o" -o test-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-ug
[ 86%] Linking CXX executable test-yaspgrid-yaspfactory-3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/test-yaspgrid-yaspfactory-3d.cc.o" -o test-yaspgrid-yaspfactory-3d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-yaspgrid-yaspfactory-3d
[ 86%] Linking CXX executable test-geogrid-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-uggrid.dir/test-geogrid.cc.o" -o test-geogrid-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-geogrid-uggrid
[ 86%] Linking CXX executable test-geogrid-yaspgrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-yaspgrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-yaspgrid.dir/test-geogrid.cc.o" -o test-geogrid-yaspgrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 86%] Built target test-geogrid-yaspgrid
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1822: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:546: build_tests] Error 2
==> ERROR: A failure occurred in check().
    Aborting...
```

`4a9b6d81d1ba84ab1158e6ba386e2f12687379db` ✅

<details>
  <summary>
  `ff1a5e1fd1ec383eece853be89843a9f87e165b9` ❌
  </summary>

```
[ 73%] Building CXX object dune/grid/test/CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/sbin/g++ -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DModelP -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-grid-git/src/build-cmake -I/tmp/makepkg/dune-grid-git/src/dune-grid -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/grid/test/CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o -MF CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o.d -o CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o -c /tmp/makepkg/dune-grid-git/src/dune-grid/dune/grid/test/issue-53-uggrid-intersections.cc
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_2d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/build.make:110: dune/grid/io/file/dgfparser/test/test-dgf-alberta] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:9104: dune/grid/io/file/dgfparser/test/CMakeFiles/test-dgf-alberta.dir/all] Error 2
make[2]: *** Waiting for unfinished jobs....
[ 73%] Linking CXX executable test-loadbalancing
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-loadbalancing.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-loadbalancing.dir/test-loadbalancing.cc.o" -o test-loadbalancing  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target test-loadbalancing
[ 73%] Linking CXX executable gmshtest-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/gmshtest-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-uggrid.dir/gmshtest.cc.o" -o gmshtest-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target gmshtest-uggrid
[ 73%] Linking CXX executable test-dgf-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-ug.dir/test-dgf-ug.cc.o" -o test-dgf-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target test-dgf-ug
[ 73%] Linking CXX executable test-dgf-gmsh-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/dgfparser/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-dgf-gmsh-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-dgf-gmsh-ug.dir/test-dgf-ug.cc.o" -o test-dgf-gmsh-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
[ 73%] Linking CXX executable gmshtest-alberta3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/io/file/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/gmshtest-alberta3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/gmshtest-alberta3d.dir/gmshtest.cc.o" -o gmshtest-alberta3d  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi ../../../../../lib/libdunealbertagrid3d.so ../../../../../lib/libdunegrid.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/libalberta_3d.so /usr/lib/libalberta_utilities.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 73%] Built target test-dgf-gmsh-ug
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlinit'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlerror'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlopenext'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlmakeresident'
/usr/sbin/ld: /usr/lib/libalberta_3d.so: undefined reference to `lt_dlsym'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/build.make:110: dune/grid/io/file/test/gmshtest-alberta3d] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:8999: dune/grid/io/file/test/CMakeFiles/gmshtest-alberta3d.dir/all] Error 2
[ 78%] Linking CXX executable test-yaspgrid-yaspfactory-1d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-1d.dir/test-yaspgrid-yaspfactory-1d.cc.o" -o test-yaspgrid-yaspfactory-1d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-yaspgrid-yaspfactory-1d
[ 78%] Linking CXX executable issue-53-uggrid-intersections
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/issue-53-uggrid-intersections.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/issue-53-uggrid-intersections.dir/issue-53-uggrid-intersections.cc.o" -o issue-53-uggrid-intersections  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target issue-53-uggrid-intersections
[ 78%] Linking CXX executable test-oned
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-oned.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-oned.dir/test-oned.cc.o" -o test-oned  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-oned
[ 78%] Linking CXX executable test-parallel-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-parallel-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-parallel-ug.dir/test-parallel-ug.cc.o" -o test-parallel-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-parallel-ug
[ 78%] Linking CXX executable test-yaspgrid-yaspfactory-2d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-2d.dir/test-yaspgrid-yaspfactory-2d.cc.o" -o test-yaspgrid-yaspfactory-2d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 78%] Built target test-yaspgrid-yaspfactory-2d
[ 82%] Linking CXX executable structuredgridfactorytest
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/utility/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/structuredgridfactorytest.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread CMakeFiles/structuredgridfactorytest.dir/structuredgridfactorytest.cc.o -o structuredgridfactorytest  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target structuredgridfactorytest
[ 82%] Linking CXX executable test-ug
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-ug.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-ug.dir/test-ug.cc.o" -o test-ug  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-ug
[ 82%] Linking CXX executable test-yaspgrid-yaspfactory-3d
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test/yasp && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-yaspgrid-yaspfactory-3d.dir/test-yaspgrid-yaspfactory-3d.cc.o" -o test-yaspgrid-yaspfactory-3d  -Wl,-rpath,/usr/lib/openmpi /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/libdunecommon.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-yaspgrid-yaspfactory-3d
[ 82%] Linking CXX executable test-geogrid-uggrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-uggrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-uggrid.dir/test-geogrid.cc.o" -o test-geogrid-uggrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-geogrid-uggrid
[ 82%] Linking CXX executable test-geogrid-yaspgrid
cd /tmp/makepkg/dune-grid-git/src/build-cmake/dune/grid/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/test-geogrid-yaspgrid.dir/link.txt --verbose=1
/usr/sbin/g++ -std=c++17 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -pthread "CMakeFiles/test-geogrid-yaspgrid.dir/test-geogrid.cc.o" -o test-geogrid-yaspgrid  -Wl,-rpath,/tmp/makepkg/dune-grid-git/src/build-cmake/lib:/usr/lib/openmpi /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunegrid.so -lquadmath /usr/lib/libgmp.so /usr/lib/libdunegeometry.so /usr/lib/libduneuggrid.so /usr/lib/openmpi/libmpi.so /usr/lib/libdunecommon.so /usr/lib/liblapack.so /usr/lib/libblas.so 
make[3]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
[ 82%] Built target test-geogrid-yaspgrid
make[2]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1822: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-grid-git/src/build-cmake'
make: *** [Makefile:546: build_tests] Error 2
==> ERROR: A failure occurred in check().
    Aborting...
```

</details>

`75fb27baa3dc38c3be3dcf3a42086b3e4162c180` ✅